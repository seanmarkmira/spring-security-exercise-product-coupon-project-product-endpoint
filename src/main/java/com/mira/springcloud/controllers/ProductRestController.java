package com.mira.springcloud.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.mira.springcloud.controllers.dto.Coupon;
import com.mira.springcloud.model.Product;
import com.mira.springcloud.repos.ProductRepo;

import jakarta.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/productapi")
public class ProductRestController {
	@Autowired
	private ProductRepo repo;
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${couponService.url}")
	private String couponServiceURL;
	
	@Autowired
	private HttpServletRequest request;
	
	@PostMapping("/products")
	public Product create(@RequestBody Product product) {
		//Get current request header Authorization and then place it to our request to couponServiceURL
		String authorizationHeader = request.getHeader("Authorization");
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", authorizationHeader);

		Coupon coupon = restTemplate.exchange(couponServiceURL+product.getCouponCode(), HttpMethod.GET, new HttpEntity<>(headers), Coupon.class).getBody();
		product.setPrice(product.getPrice().subtract(coupon.getDiscount()));
		System.out.println(coupon);
		return repo.save(product);
	}
	
	@GetMapping("/products/{productId}")
	public Product getProduct(@PathVariable String productId) {
		Product findById = repo.findById(productId);
		return findById;
	}	
}
