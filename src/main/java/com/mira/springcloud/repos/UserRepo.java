package com.mira.springcloud.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mira.springcloud.model.User;

public interface UserRepo extends JpaRepository<User, Long> {
	User findByEmail(String email);
}
