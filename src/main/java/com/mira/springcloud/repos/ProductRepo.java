package com.mira.springcloud.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mira.springcloud.model.Product;

public interface ProductRepo extends JpaRepository<Product,Long> {

	Product findById(String productId);
	
}
