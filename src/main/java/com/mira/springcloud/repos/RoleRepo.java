package com.mira.springcloud.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mira.springcloud.model.Role;

public interface RoleRepo extends JpaRepository<Role, Long> {

}
